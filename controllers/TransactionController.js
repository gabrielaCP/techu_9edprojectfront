const requestJson = require('request-json');
const crypt =require('../crypt');
const jwtFile = require('../jwt');
const dateTime = require('node-datetime');
const jwt = require('jsonwebtoken');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechu9pro/collections/";
const mlabApiKey = "apiKey=" + process.env.MLAB_API_KEY;

function createTransaction(req, res){
  console.log('POST /v1/users/:id/account/:id1/transactions');

  var token = req.headers['token'];

  //Validación del token:
  var resultToken= jwtFile.verifyToken(token);
  console.log(JSON.stringify(resultToken));
  var estado = resultToken.status;

  if(estado !=200){
    //Se termina la ejecución devolviendo el error correspondiente
    res.send(resultToken);

  }else{
    //Creamos el movimiento
    createApunteMovimiento(req, res);
  }
}

function createApunteMovimiento(req, res){

  //Validamos que los datos informados del movimiento son correctos.
  var newTransaction = validarMovimiento(req, res);
  var error = newTransaction.err;
  if(error == null){
  //Validamos que la cuenta está activa: que existe y no está bloqueada
  var customerId = req.params.customerId;
  var accountId = req.params.accountId;
  console.log("customerId: "+customerId);

  var queryAccount ='q={"$and": [{"customerId":'+customerId+'},{"accountId": '+accountId+'}]}';

  console.log(baseMlabURL+"account/?"+queryAccount+"&"+mlabApiKey);

  //Consultamos la cuenta
  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("account/?"+queryAccount+"&"+mlabApiKey,
       function(err, resMLab, body){
         if(err || body.length ==0){
           var response = {
               "msg" : "Error obteniendo la cuenta del usuario"
           }
           res.status(409).send(response);
         }else{
           console.log("Existe la cuenta");
           console.log(body[0]);
           if(body[0].blocked == null || body[0].blocked== false){
             //La cuenta no está bloqueada y se puede operar
             var saldo= Number(body[0].balance.amount);
             var balanceFinal = (req.body.transactionType != null && req.body.transactionType == "INGRESO")? (saldo +Number(req.body.balance.amount)):(saldo -Number(req.body.balance.amount));
             console.log("balanceFinal: "+balanceFinal);
             //Creamos el JSON para modificar el saldo de la cuenta
             var putBody='{"$set":{"balance": {"amount": '+balanceFinal+', "currency":"EUR"}}}';

             httpClient.put("account/?"+queryAccount+"&"+mlabApiKey, JSON.parse(putBody), function (errPut, resMLabPut, bodyPut){
               if (errPut){
                 var response = {"msg":"Error modificando el saldo de la cuenta"};
                 res.status(409);
                 res.send(response);
               }else{
                 console.log(JSON.stringify(newTransaction));

                 httpClient.post("transaction/?"+mlabApiKey, newTransaction,
                       function(errTransaction, resMLabT, bodyTransaction){
                         if(err){
                             var response = {
                               "msg" : "Se ha producido un error creando movimiento"
                             }
                             console.log(resMLabT);
                             res.status(500).send(response);
                         }else{
                              console.log("Movimiento creado correctamente");

                              res.status(201).send({
                                  "msg" : "Movimiento registrado con éxito",
                                  "transactionId": newTransaction.transactionId});
                         }
                  })
               }
           })

         }else{
           var response = {
               "msg" : "La cuenta está bloqueada"
           }
           res.status(409).send(response);
         }
      }

  });
}
}

function validarMovimiento(req, res){
  //Validamos parámetros obligatorios: customerId, accountId y balance.amount: el importe debe ser positivo
  if(req.params.customerId == null || req.params.accountId == null || req.body.balance == null){
    var response = {
      "err": "Error",
      "msg" : "Parámetros obligatorios no informados"
    }
    console.log("MAAAAL");
    console.log("req.params.customerId "+req.params.customerId);
    console.log("req.params.accountId "+req.params.accountId);
    console.log("req.body "+JSON.stringify(req.body));
    res.status(400).send(response);
    return response;
  }else if(req.body.balance.amount == null || req.body.balance.amount ==0){

    var response = {
      "err": "Error",
      "msg" : "Importe no permitido"
    }
    console.log("Importe cero o nulo");
    res.status(400).send(response);
    return response;

  }else{
    //Datos minimos obligatorios correctos
    console.log("Generamos la estructura del movimiento a insertar correcta en BBDD");
    var negativo = -1;
    var newTransactionBasicData= {
      "accountId": Number(req.params.accountId),
      "transactionId": getRandomInt(dateTime.create(new Date()).now(),(dateTime.create(new Date()).now())+100000000),
      "originAmount":{
        "amount": (req.body.transactionType != null && req.body.transactionType == "INGRESO")? req.body.balance.amount: (negativo*req.body.balance.amount),
        "currency": (req.body.balance.currency != null)? req.body.balance.currency : "EUR"
      },
      "operationDate": new Date(),
      "operationType": (req.body.transactionType != null && req.body.transactionType == "INGRESO")? "ABONO": "GASTO",
      "transactionType": (req.body.transactionType != null)? req.body.transactionType : null,
      "concept": req.body.concept
    }
    var wireTransferDetail = null;
    var billDetail = null;
    var cashIncome = null;
      if(req.body.transactionType != null && req.body.transactionType == "TRANSFERENCIA"){
         wireTransferDetail= {
            "wireTransferDetail":{
              "receiver": {
                "name":(req.body.receiver != null && req.body.receiver.fullName != null )? (req.body.receiver.fullName).toUpperCase() : null,
                "IBAN": (req.body.receiver != null && req.body.receiver.IBAN != null)? (req.body.receiver.IBAN).toUpperCase() : null
              },
              "valueDate": (dateTime.create(new Date()).now())+100000000
            }
      }
    }
    var newTransaction = Object.assign(newTransactionBasicData,wireTransferDetail);

    if(req.body.transactionType != null && req.body.transactionType == "PAGO"){
      billDetail= {
        "billDetail":{
          "billReference": (req.body.billReference != null)? (req.body.billReference).toUpperCase() : null,
          "taxIssuer": (req.body.taxIssuer != null)? (req.body.taxIssuer).toUpperCase() : null,
          "valueDate": (dateTime.create(new Date()).now())+100000000
        }
      }
    }
    newTransaction = Object.assign(newTransaction,billDetail);

    if(req.body.transactionType != null && req.body.transactionType == "INGRESO"){
      cashIncome= {
        "incomeDetail":{
          "operationDate": (dateTime.create(new Date()).now()),
          "valueDate": (dateTime.create(new Date()).now())+100000000
        }
      }
    }
    newTransaction = Object.assign(newTransaction,cashIncome);

    return newTransaction;
    }
  }


function listTransactions(req, res){
  console.log('/v1/users/:customerId/accounts/:accountId/transactions');
  var token = req.headers['token'];

  //Validación del token:
  var resultToken= jwtFile.verifyToken(token);
  console.log(JSON.stringify(resultToken));
  var estado = resultToken.status;

  if(estado !=200){
    //Se termina la ejecución devolviendo el error correspondiente
    res.send(resultToken);

  }else{
    if(req.params.customerId != null && req.params.accountId != null ){
      var customerId = req.params.customerId;
      var accountId = req.params.accountId;
      console.log("customerId: "+customerId);

      var queryAccount ='q={"$and": [{"customerId":'+customerId+'},{"accountId": '+accountId+'}]}';
      console.log(baseMlabURL+"account/?"+queryAccount+"&"+mlabApiKey);

      //Consultamos la cuenta
      var httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("account/?"+queryAccount+"&"+mlabApiKey,
           function(err, resMLab, body){
             if(err || body.length ==0){
               var response = {
                   "msg" : "Error obteniendo la cuenta del usuario"
               }
               res.status(409).send(response);
             }else{
               console.log("Existe la cuenta");
               console.log(body[0]);

               //Consultamos los movimientos:
               var queryAccountT ='q={"accountId": '+accountId+'}';
               httpClient.get("transaction/?"+queryAccountT+"&"+mlabApiKey,
                     function(errTransaction, resMLabT, bodyTransaction){
                       if(errTransaction || body.length == 0){
                         var response = {
                             "msg" : "Error obteniendo la cuenta del usuario"
                         }
                         res.status(409).send(response);
                       }else{

                         var responseAccount= {
                           "account": {
                             "accountId": body[0].accountId,
                              "alias" : body[0].alias,
                              "IBAN" : body[0].IBAN,
                              "creationDate": body[0].creationDate,
                              "productType": {
                                "id" : body[0].productType.id,
                                "name" : body[0].productType.name,
                              },
                              "balance": {
                                  "amount": body[0].balance.amount,
                                  "currency": body[0].balance.currency
                              }
                            }
                           }
                           var resultT = [];
                           var responseTransaction= {
                             "transactions": (bodyTransaction.length >0)? bodyTransaction: resultT
                           };
                           var response = Object.assign(responseAccount,responseTransaction);
                            res.status(200).send(response);

                       }
                     });
             }
          });
        }else{
          var response = {
              "msg" : "Parámetros oblogatorios no informados"
          }
          res.status(400).send(response)
        }
      }
}

function getTransaction(req, res){
  console.log('/v1/users/:id/accounts/:id1/transactions/:id3');
  var token = req.headers['token'];

  //Validación del token:
  var resultToken= jwtFile.verifyToken(token);
  console.log(JSON.stringify(resultToken));
  var estado = resultToken.status;

  if(estado !=200){
    //Se termina la ejecución devolviendo el error correspondiente
    res.send(resultToken);

  }else{
    if(req.params.customerId != null && req.params.accountId != null && req.params.transactionId != null ){
      var customerId = req.params.customerId;
      var accountId = req.params.accountId;
      var transactionId = req.params.transactionId;
      console.log("customerId: "+customerId);

      var queryAccount ='q={"$and": [{"customerId":'+customerId+'},{"accountId": '+accountId+'}]}';
      console.log(baseMlabURL+"account/?"+queryAccount+"&"+mlabApiKey);

      //Consultamos la cuenta
      var httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("account/?"+queryAccount+"&"+mlabApiKey,
           function(err, resMLab, body){
             if(err || body.length ==0){
               var response = {
                   "msg" : "Error obteniendo la cuenta del usuario"
               }
               res.status(409).send(response);
             }else{
               console.log("Existe la cuenta del usuario");
               console.log(body[0]);

               //Consultamos los movimientos:
               var queryAccountT ='q={"transactionId": '+transactionId+'}';
               httpClient.get("transaction/?"+queryAccountT+"&"+mlabApiKey,
                     function(errTransaction, resMLabT, bodyTransaction){
                       if(errTransaction || body.length == 0){
                         var response = {
                             "msg" : "Error obteniendo el movimiento"
                         }
                         res.status(409).send(response);
                       }else{

                         var responseAccount= {
                           "account": {
                              "alias" : body[0].alias,
                              "IBAN" : body[0].IBAN,
                              "creationDate": body[0].creationDate,
                              "productType": {
                                "id" : body[0].productType.id,
                                "name" : body[0].productType.name,
                              },
                              "balance": {
                                  "amount": body[0].balance.amount,
                                  "currency": body[0].balance.currency
                              }
                            }
                           }
                           var responseTransaction= {"transactions": bodyTransaction};
                           var response = Object.assign(responseAccount,responseTransaction);
                            res.status(200).send(response);

                       }
                     });
             }
          });
        }else{
          var response = {
              "msg" : "Parámetros oblogatorios no informados"
          }
          res.status(400).send(response)
        }
  }
}
function deleteTransaction(req, res){
  console.log('/v1/users/:id/accounts/:id2/transactions/:id3');
}
/**
* Genera un número aleatorio
* @param {double} min valor mínimo
* @param {double} max valor máximo
* @returns {double} número aleatorio
*/
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

/**
* Genera un número aleatorio
* @param {integer} longitud longitud del valor aleatorio
* @returns {String} valor alfanumérico generado
*/
function getRandom(longitud){
  var caracteres = "0123456789";
  var salida = "";
  for (i=0; i<longitud; i++) salida += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
  return salida;
}

module.exports.createTransaction = createTransaction;
module.exports.getTransaction = getTransaction;
module.exports.listTransactions = listTransactions;
module.exports.deleteTransaction = deleteTransaction;
