require('dotenv').config(); //Para que reconozca el .env con las propiedades

//Dependencias
var express =require('express');
var cors =require('cors');

var app = express();
const bodyParser= require('body-parser');

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

 next();
}
app.options('*',cors());
app.use(cors({exposedHeaders:['token']}));
app.use(enableCORS);

app.use(bodyParser.json());
//Uso Dependencias
app.use(express.json());
//Permitir la cabecera 'token' para que no la considere insegura





const puerto = process.env.PORT||3000;
app.listen(puerto);
console.log("API Escuchando en puerto: "+puerto);

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
const transactionController = require('./controllers/TransactionController');

console.log("API con userController y authController");

//Operativa usuarios
app.post('/v1/users', userController.createUser);
app.get('/v1/users/', userController.listUsers);
app.get('/v1/users/:customerId', userController.getUser);
app.put('/v1/users/:customerId',userController.modifyUser);
console.log("API con operativa usuarios declarada");

//Login y logout
app.post('/v1/login',authController.login);
app.put('/v1/logout/:customerId',authController.logout);
console.log("API con operativa de autenticación declarada");

//Operativa cuentas

app.post('/v1/users/:customerId/accounts',accountController.createAccount);
app.get('/v1/users/:customerId/accounts',accountController.listAccount);
app.get('/v1/users/:customerId/accounts/:accountId',accountController.getAccount);
app.delete('/v1/users/:customerId/accounts/:accountId',accountController.deleteAccount);

console.log("API con operativa de cuentas declarada");

//Movimientos

app.post('/v1/users/:customerId/accounts/:accountId/transactions',transactionController.createTransaction);
app.get('/v1/users/:customerId/accounts/:accountId/transactions',transactionController.listTransactions);
app.get('/v1/users/:customerId/accounts/:accountId/transactions/:transactionId',transactionController.getTransaction);
app.delete('/v1/users/:customerId/accounts/:accountId/transactions/:transactionId',transactionController.deleteTransaction);

console.log("API con operativa de movimientos declarada");

console.log("Ningún problema en el server");
