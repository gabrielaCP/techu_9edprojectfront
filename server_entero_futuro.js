//Dependencias
var express =require('express');
var cors =require('cors');

var app = express();

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 //OJO ,hay que incluir las cabeceras. Si utilizamos cabeceas no estandar hay que definirlas.NOEMI
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}

//Uso Dependencias
app.use(express.json());
app.use(enableCORS);

const puerto = process.env.PORT||3000;
console.log("API Escuchando en puerto port de Gabi: "+puerto);
app.listen(puerto);
console.log("API Escuchando en puerto port de Gabi: "+puerto);

const userController = require('./controllers/UserController');

console.log("API con userController");
//const authController = require('./controllers/AuthController');
//const accountController = require('./controllers/AccountController');
//const transactionController = require('./controllers/TransactionController');


//APIs disponibles:
//Login y logout
//app.post('/v1/login',authController.login);
//app.delete('/v1/logout/:id',authController.logout);

//Operativa usuarios
app.post('/v1/users', userController.createUser);
app.get('/v1/users/:id', userController.getUser);
console.log("API con operativa usuarios declarada");
//Operativa cuentas
/*
app.post('/v1/users/:id/accounts',accountController.createAccount);
app.get('/v1/users/:id/accounts',accountController.getAccounts);
app.get('/v1/users/:id1/accounts/id2',accountController.getAccount);
app.delete('/v1/users/:id1/accounts/:id2',accountController.deleteAccount);

//Movimientos

app.post('/v1/users/:id1/accounts/:id2/transactions',transactionController.createTransaction);
app.delete('/v1/users/:id1/accounts/:id2/transactions/:id3',transactionController.deleteTransaction);
app.get('/v1/users/:id1/accounts/:id2/transactions',transactionController.getTransactions);


//FIN
*/

//Los de Noemí:

/*
app.post('/v0/sessions',sessionController.login);
app.delete('/v0/sessions/:id',sessionController.logout);
app.post('/v0/users',userController.createUser);
app.put('/v0/users/:id',userController.modifyUser);
app.get('/v0/users/:id',userController.getUser);
app.post('/v0/users/:id/accounts',accountController.createAccount);
app.get('/v0/users/:id/accounts',accountController.getAccounts);
app.delete('/v0/users/:id1/accounts/:id2',accountController.deleteAccount);
app.post('/v0/users/:id1/accounts/:id2/transactions',transactionController.createTransaction);
app.delete('/v0/users/:id1/accounts/:id2/transactions/:id3',transactionController.deleteTransaction);
app.get('/v0/users/:id1/accounts/:id2/transactions',transactionController.getTransactions);
*/
console.log("Ningún problema en el server");
