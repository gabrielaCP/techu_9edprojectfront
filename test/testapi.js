const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
const expect = chai.expect();

describe('Test de API usuarios',
  function (){
    it('Listar usuarios', function(done){
        chai.request('http://localhost:3000')
        .get('/v1/users/')
        .end(function(err,res){
            console.log(res);
            console.log("Request has finished");
            res.should.have.status(200);
            done();
          }
        )
      }
    )
  }
)
